-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256


Last update: 2023-01-07

In this file you can find my current OMEMO fingerprints from all the clients I use.

Every other fingerprint that isn't on this file is not used.


- -getimiskon@getimiskon.xyz

 This is my main XMPP account, hosted on my server.

 Desktop clients:

 Profanity: 3d2c27b4 0b3a4b2a 7a1001d7 169fc8b9 8b13e451 d1c01504 f0c0c026 b44a0f32

 Mobile clients:

 Conversations: d93c2ab3 eccdb80a fc51acb8 32d2f47e 76101e8c 75b0ab25 ef046737 fa8de975


- -getimiskon@chaox.ro

 This is my alternative server, which I'll use in case my main XMPP server is down.

 Desktop clients:

 Profanity: fdd3361f 9d788fdf 51653e92 aa85a2a2 e3772dc9 1ad62626 b409eaa1 69674b4f

 Mobile clients:

 Conversations: cd8e8355 b97608e9 8dcb7ac7 7e5d5939 5b634215 c1175ba7 fe8f5627 caa70a1b


- -getimiskon@snopyta.org

 I will use this account in case both the servers above are down.
 I usually avoid using this account because of spam issues.

 Desktop clients:

 Profanity: 53e9df26 e4e6dfca 95bd2b11 2afca383 9a46bb79 dc5b5950 67c91721 09752041
 Dino: 44a85163 eb1870ef 9c9edec7 f20dccaf 4f31f5f8 d9908e0c b020dc81 90fcaa73

 Mobile clients:

 Conversations: b6048039 8496984a 9a49f5a1 05c57dde 85d0a5bc 6664d462 9994eb06 667a1211

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEQZvwC9nkwVxkpTB/cnwTI72hBbsFAmO55lEACgkQcnwTI72h
Bbu/8w/7B14dAfI7wWdvVQcjthp3WvXp02uQo90OnMbFyg3h6uIullTmJSFJjLzF
n3/ddXlL+fRUzMiyBC/e96qmU7hqNYwc+1efwannelX4rN205YawbFyQ8BN1+TW3
CZRevJET7UTBqtjo07HCAn3vMxAmC0JgChrrgYz0LNU5dhBmziY7boCfjWG3Axj8
l/tcYHibH+tMLW0c2YVo1ha1Y6kpIMOKjpQHbiywWyB+n5zo9aWs1priLCE4Y6xH
4nj+cUa6489VsoO09FoGVlFvzoCv6/13RWeFrAatenNV++YTsrAAGXISzPmQE7ci
CTwoVk3Uk0InFuwnsYrVC3zTH3xqXq08A2IoVIA8kJLXeBnu1FKFeMXm0tQ5QD9+
0hE6EM8E6YlrJc/OWCIWzkCBN97Fb1GZJU9FrR97Tsxxw6DU2lf0/YcdjnPwl779
dNyV+48yh77XkApmiuqdj7TyEnShEDjI/tBdtsSXN8kc9+yEFkioTJ3yAlTlw8h1
kK5qo2ScfRqmjNqY5/hkEWvjEsiWTooaZhZBgqhS7vW/IaXNmQLFyyvSon/kLb5E
8LG2oplIQrm/0VeD2wRfv9f9W8X7B/zS5PxSsNpg8qujMn3TssUyb9K7Z//b9uHI
iP1Hzc9o5MjG99ukeuZyiB+2qzkmNclRDkOjhEEMnik6RcH/MEc=
=7KWk
-----END PGP SIGNATURE-----
